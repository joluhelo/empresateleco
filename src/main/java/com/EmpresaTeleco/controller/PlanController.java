package com.EmpresaTeleco.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.EmpresaTeleco.model.Plan;
import com.EmpresaTeleco.service.PlanService;

@RestController
@RequestMapping("api/empresa/plan")
public class PlanController {

	@Autowired
	private PlanService planService;
	
	@PostMapping(value = "post")
	public Plan agregarPlan(@RequestBody Plan plan) {
		return planService.agregarPlan(plan);
	}
	
}
