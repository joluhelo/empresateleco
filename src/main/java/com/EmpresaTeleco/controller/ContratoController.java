package com.EmpresaTeleco.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.EmpresaTeleco.model.Contrato;
import com.EmpresaTeleco.service.ContratoService;


@RestController
@RequestMapping("api/empresa/contrato")
public class ContratoController {
	
	@Autowired
	private ContratoService contratoService;
	
	@PostMapping(value = "post")
	public Contrato agregarContrato(@RequestBody Contrato contrato) {
		return contratoService.agregarContrato(contrato);
	}
	
	@GetMapping(value = "get-contrato")
	public List<Contrato> contratoMostrar(){
		return contratoService.contratoMostrar();
	}
	
	@PostMapping(value="set-contrato")
	public Contrato agregar(@RequestBody Contrato contrato) {
		return contratoService.agregar(contrato);
	}	
	
	@PutMapping(value="edit-contrato")
	public Contrato modificar(@RequestBody Contrato contrato ) {
		return contratoService.modificar(contrato);
	}
	
	@DeleteMapping(value="delete-contrato")
	public void borrar(@RequestParam Integer id) {
		contratoService.borrar(id);
		
	}
	

}
