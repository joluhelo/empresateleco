package com.EmpresaTeleco.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.EmpresaTeleco.model.Cliente;
import com.EmpresaTeleco.service.ClienteService;


@RestController
@RequestMapping("api/empresa/")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@PostMapping(value = "post-cliente")
	public Cliente agregarCliente (@RequestBody Cliente cliente) {
		return clienteService.agregarCliente(cliente);
	}

}
