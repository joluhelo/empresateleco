package com.EmpresaTeleco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpresaTelecoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpresaTelecoApplication.class, args);
	}

}
