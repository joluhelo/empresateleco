package com.EmpresaTeleco.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.EmpresaTeleco.model.Contrato;
import com.EmpresaTeleco.repo.ContratoDao;

@Service 
public class ContratoService {
	
	@Autowired
	private ContratoDao contratoDao;
	public Contrato agregarContrato(Contrato contrato) {
		return contratoDao.save(contrato);
	}
	
	public List<Contrato> contratoMostrar() {
		return contratoDao.findAll();
	}
	
	public Contrato agregar(@RequestBody Contrato contratos) {
		return contratoDao.save(contratos);
	}

	public Contrato modificar(@RequestBody Contrato contrato) {
			return contratoDao.save(contrato);
	}
	
	public void borrar(@RequestParam Integer id) {
		contratoDao.deleteById(id);
	}

}
