package com.EmpresaTeleco.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.EmpresaTeleco.model.Plan;
import com.EmpresaTeleco.repo.PlanDao;

@Service
public class PlanService {
	
	@Autowired
	private PlanDao planDao;
	
	public Plan agregarPlan(Plan plan) {
		return planDao.save(plan);
	}

}
