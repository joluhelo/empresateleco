package com.EmpresaTeleco.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.EmpresaTeleco.model.Cliente;
import com.EmpresaTeleco.repo.ClienteDao;

@Service 
public class ClienteService {
	
	@Autowired
	private ClienteDao clienteDao;
	
	public Cliente agregarCliente(Cliente cliente) {
		return clienteDao.save(cliente);
	}
	

}
