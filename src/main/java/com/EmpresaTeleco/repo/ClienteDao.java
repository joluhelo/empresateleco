package com.EmpresaTeleco.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.EmpresaTeleco.model.Cliente;

public interface ClienteDao extends JpaRepository<Cliente, Integer>{
	
		

}
