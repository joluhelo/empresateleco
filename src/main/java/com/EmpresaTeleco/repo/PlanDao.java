package com.EmpresaTeleco.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.EmpresaTeleco.model.Plan;

public interface PlanDao extends JpaRepository<Plan, Integer>{
	

}
