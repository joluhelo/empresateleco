package com.EmpresaTeleco.repo;



import org.springframework.data.jpa.repository.JpaRepository;


import com.EmpresaTeleco.model.Contrato;

public interface ContratoDao extends JpaRepository<Contrato, Integer>{	


}
